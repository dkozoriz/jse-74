package ru.t1.dkozoriz.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.dkozoriz.tm.component.Bootstrap;
import ru.t1.dkozoriz.tm.configuration.LoggerConfiguration;

public class Application {
    public static void main(@Nullable final String... args) {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}