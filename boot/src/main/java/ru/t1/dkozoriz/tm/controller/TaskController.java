package ru.t1.dkozoriz.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.dkozoriz.tm.dto.CustomUser;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.model.Task;
import ru.t1.dkozoriz.tm.repository.ProjectRepository;
import ru.t1.dkozoriz.tm.service.TaskService;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectRepository projectRepository;

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/task/create")
    public String create(
            @AuthenticationPrincipal final CustomUser user
    ) {
        taskService.create(user.getUserId(), "New Task" + System.currentTimeMillis(), "");
        return "redirect:/tasks";
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/task/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        taskService.deleteById(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        final Task task = taskService.findById(user.getUserId(), id);
        return new ModelAndView("task-edit", "task", task);
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/task/edit/{id}")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("task") Task task, BindingResult result
    ) {
        taskService.save(user.getUserId(), task);
        return "redirect:/tasks";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @ModelAttribute("projects")
    public Collection<Project> getProjects(@AuthenticationPrincipal final CustomUser user) {
        return projectRepository.findAllByUserId(user.getUserId());
    }

}