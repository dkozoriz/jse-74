package ru.t1.dkozoriz.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.dkozoriz.tm.dto.CustomUser;
import ru.t1.dkozoriz.tm.service.TaskService;

@Controller
public class TasksController {

    @Autowired
    private TaskService taskService;


    @PreAuthorize("isAuthenticated()")
    @GetMapping("/tasks")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) {
        return new ModelAndView("task-list", "tasks", taskService.findAll(user.getUserId()));
    }

}