package ru.t1.dkozoriz.tm.api.service;

import ru.t1.dkozoriz.tm.api.service.dto.ISessionDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.IUserDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.business.IProjectDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.business.ITaskDtoService;
import ru.t1.dkozoriz.tm.api.service.model.ISessionService;
import ru.t1.dkozoriz.tm.api.service.model.IUserService;
import ru.t1.dkozoriz.tm.api.service.model.business.IProjectService;
import ru.t1.dkozoriz.tm.api.service.model.business.ITaskService;

public interface IServiceLocator {

    IProjectDtoService getProjectDtoService();

    ITaskDtoService getTaskDtoService();

    IUserDtoService getUserDtoService();

    IAuthService getAuthService();

    IPropertyService getPropertyService();

    ISessionDtoService getSessionDtoService();

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    ISessionService getSessionService();

}