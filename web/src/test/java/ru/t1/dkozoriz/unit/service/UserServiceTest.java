package ru.t1.dkozoriz.unit.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.marker.UnitCategory;
import ru.t1.dkozoriz.tm.configuration.DataConfiguration;
import ru.t1.dkozoriz.tm.enumerated.RoleType;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.service.UserService;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataConfiguration.class})
@Category(UnitCategory.class)
public class UserServiceTest {

    private final User user1 = new User();

    private final User user2 = new User();

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test_user", "test_user");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        user1.setLogin("test_login1");
        user1.setPasswordHash("test_password1");
        user2.setLogin("test_login2");
        user2.setPasswordHash("test_password2");
        userService.save(user1);
        userService.save(user2);
    }

    @After
    public void clean() {
        userService.deleteById(user1.getId());
        userService.deleteById(user2.getId());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(5, userService.findAll().size());
    }

    @Test
    public void findByIdTest() {
        final User user = userService.findById(user1.getId());
        Assert.assertEquals(user.getId(), user1.getId());
        Assert.assertEquals(user.getLogin(), user1.getLogin());
        Assert.assertEquals(user.getPasswordHash(), user1.getPasswordHash());
    }

    @Test
    public void addTest() {
        User user = new User();
        user.setLogin("new_user");
        user.setPasswordHash("new_password");
        userService.save(user);
        Assert.assertEquals(6, userService.count());
    }

    @Test
    public void updateTest() {
        User user = new User();
        user.setLogin("new_user");
        user.setPasswordHash("new_password");
        Assert.assertNull(userService.update(user));
        userService.save(user);
        user.setLogin("new_name");
        userService.update(user);
        Assert.assertEquals("new_name", userService.findById(user.getId()).getLogin());
    }

    @Test
    public void createTest() {
        userService.createUser("test_user", "test_description", RoleType.USER);
        Assert.assertEquals(6, userService.count());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(userService.findAll().size(), userService.count());
    }

    @Test
    public void deleteByIdTest() {
        long count = userService.count();
        userService.createUser("new_user", "new_description", RoleType.USER);
        User user = userService.findByLogin("new_user");
        userService.deleteById(user.getId());
        Assert.assertEquals(count, userService.count());
    }

    @Test
    public void LockByIdTest() {
        userService.lockUser(user2.getId());
        Assert.assertEquals(true, userService.findById(user2.getId()).getLocked());
    }

    @Test
    public void unlockByIdTest() {
        userService.unlockUser(user2.getId());
        Assert.assertEquals(false, userService.findById(user2.getId()).getLocked());
    }

}