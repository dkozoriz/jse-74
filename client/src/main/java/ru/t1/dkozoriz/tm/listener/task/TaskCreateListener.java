package ru.t1.dkozoriz.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.task.TaskCreateRequest;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class TaskCreateListener extends AbstractTaskListener {

    public TaskCreateListener() {
        super("task-create", "create new task.");
    }

    @Override
    @EventListener(condition = "@taskCreateListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        taskEndpoint.taskCreate(new TaskCreateRequest(getToken(), name, description));
    }

}
