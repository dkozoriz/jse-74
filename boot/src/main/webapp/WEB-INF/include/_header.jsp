<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
    <title>TASK MANAGER</title>
</head>
<style>

    h1 {
        font-size: 20px;
    }

    table {
        width: 100%;
    }

    td, th {
        border: 1px solid #ededed;
        text-align: left;
    }

    a.menu {
        text-decoration: none;
        margin-right:20px;
    }
    a.menu2 {
        text-decoration: none;
        margin-right:20px;
        text-align: right;
    }

</style>
<body>


    <a class="menu" href="/">MAIN</a>
    <a class="menu" href="/projects">PROJECTS</a>
    <a class="menu" href="/tasks">TASKS</a>
    <a class="menu2" href="/login">LOGIN</a>

<br/>
</body>