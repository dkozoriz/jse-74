package ru.t1.dkozoriz.tm.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.dkozoriz.tm.api.service.dto.ISessionDtoService;
import ru.t1.dkozoriz.tm.dto.model.SessionDto;
import ru.t1.dkozoriz.tm.repository.dto.SessionDtoRepository;

@Service
@NoArgsConstructor
public class SessionDtoService extends UserOwnedDtoService<SessionDto> implements ISessionDtoService {

    private final static String NAME = "Session";

    @NotNull
    @Autowired
    @Getter
    private SessionDtoRepository repository;

    @NotNull
    public String getName() {
        return NAME;
    }

}