package ru.t1.dkozoriz.tm.repository.model.business;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.model.business.Project;

@Repository
@Scope("prototype")
public interface ProjectRepository extends BusinessRepository<Project> {

}