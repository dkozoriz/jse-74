package ru.t1.dkozoriz.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.dkozoriz.tm.dto.CustomUser;
import ru.t1.dkozoriz.tm.service.ProjectService;

@Controller
public class ProjectsController {

    @Autowired
    private ProjectService projectService;


    @PreAuthorize("isAuthenticated()")
    @GetMapping("/projects")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) {
        return new ModelAndView("project-list", "projects", projectService.findAll(user.getUserId()));
    }

}