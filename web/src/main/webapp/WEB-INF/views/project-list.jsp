<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<jsp:include page="../include/_header.jsp"/>

<h1>PROJECT LIST</h1>
<table>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">USER ID</th>
        <th scope="col">NAME</th>
        <th scope="col">DESCRIPTION</th>
        <th scope="col">STATUS</th>
        <th scope="col">CREATED</th>
        <th scope="col">EDIT</th>
        <th scope="col">DELETE</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td><c:out value="${project.id}"/></td>
            <td><c:out value="${project.userId}"/></td>
            <td><c:out value="${project.name}"/></td>
            <td><c:out value="${project.description}"/></td>
            <td><c:out value="${project.status.displayName}"/></td>
            <td><fmt:formatDate value="${project.created}"/></td>
            <td><a href="/project/edit/${project.id}"/>EDIT</td>
            <td><a href="/project/delete/${project.id}"/>DELETE</td>
        </tr>
    </c:forEach>
</table>
<br/>

    <form action="/project/create" style="margin-top: 20px">
        <button>Create Project</button>
    </form>


<jsp:include page="../include/_footer.jsp"/>