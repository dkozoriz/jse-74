package ru.t1.dkozoriz.tm.exception.user;

import ru.t1.dkozoriz.tm.exception.field.AbstractFieldException;

public final class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! User id is empty.");
    }

}