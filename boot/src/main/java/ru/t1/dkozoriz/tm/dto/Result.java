package ru.t1.dkozoriz.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Result {

    private String errorMessage = "";

    private boolean success = true;

    public Result(final boolean success) {
        this.success = success;
    }

    public Result(final Exception e) {
        success = false;
        errorMessage = e.getMessage();
    }

}