package ru.t1.dkozoriz.tm.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.repository.ProjectRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectService {

    private final ProjectRepository repository;


    public List<Project> findAll(final String userId) {
        return repository.findAllByUserId(userId);
    }

    @Transactional
    public Project save(final String userId, Project project) {
        project.setUserId(userId);
        return repository.save(project);
    }

    @Transactional
    public Project update(final String userId, Project project) {
        if (!repository.existsById(project.getId())) return null;
        project.setUserId(userId);
        return repository.save(project);
    }

    public Project findById(final String userId, final String id) {
        return repository.findByUserIdAndId(userId, id);
    }

    public long count(final String userId) {
        return repository.countByUserId(userId);
    }

    @Transactional
    public void deleteById(final String userId, final String id) {
        repository.deleteByUserIdAndId(userId, id);
    }

    @Transactional
    public void deleteAll(final String userId) {
        repository.deleteByUserId(userId);
    }

    @Transactional
    public Project create(final String userId, final String name, final String description) {
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return repository.save(project);
    }

    public List<Project> findAll() {
        return repository.findAll();
    }

    @Transactional
    public Project save(Project project) {
        return repository.save(project);
    }

    @Transactional
    public Project update(Project project) {
        if (!repository.existsById(project.getId())) return null;
        return repository.save(project);
    }

    public Project findById(String id) {
        return repository.findById(id).orElse(null);
    }

    public long count() {
        return repository.count();
    }

    @Transactional
    public void deleteById(String id) {
        repository.deleteById(id);
    }

    @Transactional
    public void deleteAll() {
        repository.deleteAll();
    }

}