package ru.t1.dkozoriz.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.dkozoriz.tm.api.endpoint.*;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;

@Configuration
@ComponentScan("ru.t1.dkozoriz.tm")
public class ClientConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public IAuthEndpoint authEndpoint() {
        return IAuthEndpoint.newInstance(propertyService);
    }

    @Bean
    @NotNull
    public ISystemEndpoint systemEndpoint() {
        return ISystemEndpoint.newInstance(propertyService);
    }

    @Bean
    @NotNull
    public IProjectEndpoint projectEndpoint() {
        return IProjectEndpoint.newInstance(propertyService);
    }

    @Bean
    @NotNull
    public ITaskEndpoint taskEndpoint() {
        return ITaskEndpoint.newInstance(propertyService);
    }

    @Bean
    @NotNull
    public IUserEndpoint userEndpoint() {
        return IUserEndpoint.newInstance(propertyService);
    }

}