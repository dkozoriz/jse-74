package ru.t1.dkozoriz.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDto;
import ru.t1.dkozoriz.tm.dto.request.task.TaskShowAllByProjectIdRequest;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskShowByProjectIdListener extends AbstractTaskListener {

    public TaskShowByProjectIdListener() {
        super("task-show-by-project-id", "show task by project id.");
    }

    @Override
    @EventListener(condition = "@taskShowByProjectIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final List<TaskDto> tasks =
                taskEndpoint.taskShowAllByProjectId(new TaskShowAllByProjectIdRequest(getToken(), projectId)).getTaskList();
        renderTasks(tasks);
    }

}