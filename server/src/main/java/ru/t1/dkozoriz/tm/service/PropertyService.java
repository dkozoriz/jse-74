package ru.t1.dkozoriz.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

@Service
@Getter
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @Value("#{environment['server.port']}")
    private String serverPort;

    @Value("#{environment['server.host']}")
    private String serverHost;

    @Value("#{environment['application.name']}")
    private String applicationName;

    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @Value("#{environment['session.key']}")
    private String sessionKey;

    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @Value("#{environment['database.username']}")
    private String DBUsername;

    @Value("#{environment['database.password']}")
    private String DBPassword;

    @Value("#{environment['database.url']}")
    private String DBUrl;

    @Value("#{environment['database.driver']}")
    private String DBDriver;

    @Value("#{environment['database.dialect']}")
    private String DBDialect;

    @Value("#{environment['database.hbm2ddl_auto']}")
    private String DBHmb2DDLAuto;

    @Value("#{environment['database.show_sql']}")
    private String DBShowSQL;

    @Value("#{environment['database.second_lvl_cache']}")
    private String DBCacheSecondLevel;

    @Value("#{environment['database.factory']}")
    private String DBCacheFactory;

    @Value("#{environment['database.use_query_cache']}")
    private String DBCacheUseQuery;

    @Value("#{environment['database.use_min_puts']}")
    private String DBCacheUseMinPuts;

    @Value("#{environment['database.region_prefix']}")
    private String DBCacheRegionPrefix;

    @Value("#{environment['database.config_file_path']}")
    private String DBCacheConfigFilePath;

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";

    @NotNull
    private static final String APPLICATION_FILE_NAME_KEY = "application.config";

    @NotNull
    private static final String APPLICATION_LOG_KEY = "application.log";

    @NotNull
    private static final String APPLICATION_LOG_DEFAULT = "./";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String GIT_BRANCH = "gitBranch";

    @NotNull
    private static final String GIT_COMMIT_ID = "gitCommitId";

    @NotNull
    private static final String GIT_COMMITTER_NAME = "gitCommitterName";

    @NotNull
    private static final String GIT_COMMITTER_EMAIL = "gitCommitterEmail";

    @NotNull
    private static final String GIT_COMMIT_MESSAGE = "gitCommitMessage";

    @NotNull
    private static final String GIT_COMMIT_TIME = "gitCommitTime";

    @NotNull
    public static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean existConfig = isExistExternalConfig();
        if (existConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @Nullable final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    private boolean isExistExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @Override
    @NotNull
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @Override
    @NotNull
    public String getApplicationLog() {
        return getStringValue(APPLICATION_LOG_KEY, APPLICATION_LOG_DEFAULT);
    }

    @Override
    @NotNull
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getGitBranch() {
        return read(GIT_BRANCH);
    }

    @Override
    @NotNull
    public String getGitCommitId() {
        return read(GIT_COMMIT_ID);
    }

    @Override
    @NotNull
    public String getGitCommitterName() {
        return read(GIT_COMMITTER_NAME);
    }

    @Override
    @NotNull
    public String getGitCommitterEmail() {
        return read(GIT_COMMITTER_EMAIL);
    }

    @Override
    @NotNull
    public String getGitCommitMessage() {
        return read(GIT_COMMIT_MESSAGE);
    }

    @Override
    @NotNull
    public String getGitCommitTime() {
        return read(GIT_COMMIT_TIME);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

}