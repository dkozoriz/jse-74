package ru.t1.dkozoriz.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.model.SessionDto;
import ru.t1.dkozoriz.tm.dto.model.UserDto;

public interface IAuthService {

    @NotNull
    UserDto registry(@Nullable String login, @Nullable String password, @Nullable String Email) throws Exception;

    String login(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    SessionDto validateToken(@Nullable String token);

    void invalidate(@Nullable SessionDto session) throws Exception;

}