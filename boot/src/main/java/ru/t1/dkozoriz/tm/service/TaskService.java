package ru.t1.dkozoriz.tm.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.tm.model.Task;
import ru.t1.dkozoriz.tm.repository.TaskRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository repository;

    public List<Task> findAll(final String userId) {
        return repository.findAllByUserId(userId);
    }

    public Task findById(final String userId, final String id) {
        return repository.findByUserIdAndId(userId, id);
    }

    @Transactional
    public void deleteById(final String userId, final String id) {
        repository.deleteByUserIdAndId(userId, id);
    }

    @Transactional
    public Task save(final String userId, final Task task) {
        task.setUserId(userId);
        return repository.save(task);
    }

    @Transactional
    public Task update(final String userId, final Task task) {
        if (!repository.existsById(task.getId())) return null;
        task.setUserId(userId);
        return repository.save(task);
    }

    @Transactional
    public void deleteAll(final String userId) {
        repository.deleteByUserId(userId);
    }

    public long count(final String userId) {
        return repository.countByUserId(userId);
    }

    @Transactional
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return repository.save(task);
    }

    public List<Task> findAll() {
        return repository.findAll();
    }

    @Transactional
    public Task save(Task task) {
        return repository.save(task);
    }

    @Transactional
    public Task update(Task task) {
        if (!repository.existsById(task.getId())) return null;
        return repository.save(task);
    }

    public Task findById(String task) {
        return repository.findById(task).orElse(null);
    }

    public long count() {
        return repository.count();
    }

    @Transactional
    public void deleteById(String task) {
        repository.deleteById(task);
    }

    @Transactional
    public void deleteAll() {
        repository.deleteAll();
    }

}