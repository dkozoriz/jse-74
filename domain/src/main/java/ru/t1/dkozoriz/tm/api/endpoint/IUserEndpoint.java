package ru.t1.dkozoriz.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.dto.request.user.*;
import ru.t1.dkozoriz.tm.dto.response.user.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint extends IEndpoint {

    @NotNull
    String NAME = "UserEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @NotNull
    @WebMethod
    LockUserResponse userLock(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLockRequest request
    );

    @NotNull
    @WebMethod
    UnlockUserResponse userUnlock(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUnlockRequest request
    );

    @NotNull
    @WebMethod
    RemoveUserResponse userRemove(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRemoveRequest request
    );

    @NotNull
    @WebMethod
    ChangePasswordUserResponse userChangePassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserChangePasswordRequest request
    );

    @NotNull
    @WebMethod
    UpdateProfileUserResponse userUpdateProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUpdateProfileRequest request
    );

    @NotNull
    @WebMethod
    RegistryUserResponse userRegistry(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRegistryRequest request
    );

    @NotNull
    @WebMethod
    FindUserByLoginResponse userFindByLogin(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserFindByLoginRequest request
    );

}